package org.cloudtk.microservices.user.api;

import org.cloudtk.microservices.user.dto.UserDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/")
public class UserController {

    private static final List<UserDTO> USER_LIST = Arrays.asList(
        new UserDTO("John", "Snow", "jsnow"),
        new UserDTO("Daenerys", "Targaryen", "dtargaryen"),
        new UserDTO("Arya", "Stark", "astark"),
        new UserDTO("Tyrion", "Lannister", "tlannister")
    );

    @RequestMapping(method = RequestMethod.GET)
    public List<UserDTO> getUsers() {
        return USER_LIST;
    }
}
