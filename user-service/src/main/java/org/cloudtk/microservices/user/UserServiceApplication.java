package org.cloudtk.microservices.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import javax.net.ssl.HttpsURLConnection;

@EnableAutoConfiguration
@ComponentScan
@EnableEurekaClient
public class UserServiceApplication {

    public static void main(String[] args) {
        HttpsURLConnection.setDefaultHostnameVerifier((host, session) -> true);
        SpringApplication.run(UserServiceApplication.class, args);
    }

}
