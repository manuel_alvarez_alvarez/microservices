package org.cloudtk.microservices.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

import javax.net.ssl.HttpsURLConnection;

@EnableAutoConfiguration
@ComponentScan
@EnableEurekaClient
public class WebApplication {

    public static void main(String[] args) {
        HttpsURLConnection.setDefaultHostnameVerifier((host, session) -> true);
        SpringApplication.run(WebApplication.class, args);
    }
}
