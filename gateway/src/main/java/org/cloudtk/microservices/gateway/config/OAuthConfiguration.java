package org.cloudtk.microservices.gateway.config;

import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.stereotype.Component;

@Configuration
@Component
@EnableOAuth2Sso
public class OAuthConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
            .authorizeRequests()
                .anyRequest().authenticated()
                .antMatchers(HttpMethod.GET, "/api/user/**").access("#oauth2.hasScope('read')")
                .antMatchers(HttpMethod.OPTIONS, "/api/user/**").access("#oauth2.hasScope('read')")
                .antMatchers(HttpMethod.POST, "/api/user/**").access("#oauth2.hasScope('write')")
                .antMatchers(HttpMethod.PUT, "/api/user/**").access("#oauth2.hasScope('write')")
                .antMatchers(HttpMethod.PATCH, "/api/user/**").access("#oauth2.hasScope('write')")
                .antMatchers(HttpMethod.DELETE, "/api/user/**").access("#oauth2.hasScope('write')")
            .and().csrf();
        // @formatter:on
    }
}
