package org.cloudtk.microservices.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.context.annotation.ComponentScan;

import javax.net.ssl.HttpsURLConnection;

@EnableAutoConfiguration
@EnableConfigServer
@ComponentScan
public class ConfigServerApplication {

    public static void main(String[] args) {
        HttpsURLConnection.setDefaultHostnameVerifier((host, session) -> true);
        SpringApplication.run(ConfigServerApplication.class, args);
    }
}
