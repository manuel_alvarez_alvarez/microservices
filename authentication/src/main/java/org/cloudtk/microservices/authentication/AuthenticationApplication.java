package org.cloudtk.microservices.authentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

import javax.net.ssl.HttpsURLConnection;

@ComponentScan
@EnableAutoConfiguration
@EnableEurekaClient
public class AuthenticationApplication {

    public static void main(String[] args) {
        HttpsURLConnection.setDefaultHostnameVerifier((host, session) -> true);
        SpringApplication.run(AuthenticationApplication.class, args);
    }
}
