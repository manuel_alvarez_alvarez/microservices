package org.cloudtk.microservices.registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.net.ssl.HttpsURLConnection;

@EnableEurekaServer
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class ServiceRegistryApplication {

    public static void main(String[] args) {
        HttpsURLConnection.setDefaultHostnameVerifier((host, session) -> true);
        SpringApplication.run(ServiceRegistryApplication.class, args);
    }

}
